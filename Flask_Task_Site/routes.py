from flask import render_template, flash, redirect, url_for
from Flask_Task_Site import app, db
from Flask_Task_Site.forms import CreateTaskForm, MarkCompletedForm
from Flask_Task_Site.models import Task
from datetime import datetime

@app.route('/')
def pending_tasks():
    # Get tasks in descending order by creation date time
    tasks = Task.query\
        .filter_by(completed = None)\
        .order_by(Task.created.desc())



    return render_template("pending_tasks.html", title='Pending Tasks', tasks=tasks)

@app.route('/view_completed_tasks')
def view_completed():
    # Get tasks in descending order by created date time
    tasks = Task.query\
        .filter(Task.completed.isnot(None))\
        .order_by(Task.completed.desc())
    return render_template("view_completed.html", title='Completed Tasks', tasks=tasks);

@app.route('/create_task', methods=['GET', 'POST'])
def create_task():
    form = CreateTaskForm()

    # Handle POST request
    if form.validate_on_submit():
        # Show success alert
        flash(f'Task: {form.name.data} was created successfully', 'success')

        # Add task to database
        task = Task(name=form.name.data, description=form.description.data)
        db.session.add(task)
        db.session.commit()
        return redirect(url_for('pending_tasks'))

    return render_template("create_task.html", title='Create Task', form=form);

@app.route('/mark_complete/', methods=['POST'])
def mark_complete():
    form = MarkCompletedForm()

    # Handle POST request
    if form.validate_on_submit():
        # Get task from database
        task = Task.query.get_or_404(form.id.data)
        task.completed = datetime.utcnow()
        db.session.commit()

        # Show success alert
        flash(f'Task: {task.name} was marked as completed', 'success')
        return redirect(url_for('pending_tasks'))

    flash('Task failed to be marked completed', 'danger')
    return redirect(url_for('pending_tasks'))