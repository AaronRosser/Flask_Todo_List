from Flask_Task_Site import db
from Flask_Task_Site.forms import MarkCompletedForm
from datetime import datetime

class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(25), nullable=False)
    description = db.Column(db.String(150))
    created = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    completed = db.Column(db.DateTime())

    def __repr__(self):
        return f'Task("{self.name}", "{self.description}", "{self.created}", "{self.completed}")'

    def get_form(self):
        form = MarkCompletedForm()
        form.id.data = self.id
        return form
