from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, HiddenField, ValidationError
from wtforms.validators import DataRequired, Length

class CreateTaskForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=2, max=25)])
    description = TextAreaField('Description', validators=[Length(min=0, max=150)])

    submit = SubmitField('Create Task')

class MarkCompletedForm(FlaskForm):
    id = HiddenField('TaskId', validators=[DataRequired()])
    submit = SubmitField('Mark Completed')

    def validate_id(form, field):
        try:
            val = int(field.data)
            if (val < 0):
                raise ValidationError('Id must be a positive integer')
        except:
            raise ValidationError('Id must be a positive integer')
